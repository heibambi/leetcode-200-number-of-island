function numIslands (grid) {
    let islandCount = 0;
    
    for(let rowIndex in grid){
        for(let columnIndex in grid[rowIndex]){
            if(grid[rowIndex][columnIndex] === "1" ){
                islandCount++;
                
                transfromLand(parseInt(rowIndex), parseInt(columnIndex) , grid);
            }
        }
    }
    return islandCount
};

function transfromLand (rowIndex , columnIndex, grid){
    if (grid[rowIndex] == undefined){
        return;
    }
    
    if (grid[rowIndex][columnIndex] == undefined){
        return;
    }
    
    if (grid[rowIndex][columnIndex] == "0"){
        return
    }
    // first transform the island we are standing on 
    grid[rowIndex][columnIndex] = "0";

    transfromLand(rowIndex - 1 , columnIndex, grid)
    transfromLand(rowIndex + 1 , columnIndex, grid)
    transfromLand(rowIndex , columnIndex - 1, grid)
    transfromLand(rowIndex , columnIndex + 1, grid)    
}

const question1 = [
    ["1","1","1","1","0"],
    ["1","1","0","1","0"],
    ["1","1","0","0","0"],
    ["0","0","0","0","0"]
]
const question2 = [
    ["1","1","0","0","0"],
    ["1","1","0","0","0"],
    ["0","0","1","0","0"],
    ["0","0","0","1","1"]
]

console.log({
    question1 : numIslands(question1) ,
    question2 : numIslands(question2) ,
})

