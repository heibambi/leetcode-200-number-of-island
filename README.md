# leetcode-200 number of island
## Getting started
# 1 first do a nested that loop through the grid
# 2 check the the grid[x][y] == "1"
# 3 if yes then run increment the count
    # and right after also run a helper function, rendering the "island" we standing on will be "0"

# 4 the help function is a recusive function
    # first it render that current [x][y] = "0"
    # and it calls itself to render the box surrounding it to "0"
        #[x-1][y]
        #[x+1][y]
        #[x][y-1]
        #[x][y+1]
    # add checkiing : return if (basecase to stop the recusion)
        # [x] is undefined
        # [y] is undefined
        # [x][y] is already = "0"
